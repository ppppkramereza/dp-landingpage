<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LandingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@beranda');
Route::get('/about', 'LandingController@about');
Route::get('/contacts', 'LandingController@contacts');
Route::get('/news', 'LandingController@news');
Route::get('/ourteam', 'LandingController@ourteam');
Route::get('/product', 'LandingController@product');
Route::get('/broker', 'LandingController@broker');
Route::get('/privacy', 'LandingController@privacy');
Route::get('/register', 'LandingController@register');
Route::get('/register/{refferal}', 'LandingController@registerrefferal');
Route::post('/registeract', 'LandingController@registeract');
Route::get('/verifikasiemail', 'LandingController@verifikasiemail');
Route::get('/verif/{email}', 'LandingController@verif')->name('verif');
