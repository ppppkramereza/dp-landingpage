<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uplines extends Model
{
    use HasFactory;

    protected $table='uplines';

     protected $fillable = [
        'user_id',
        'upline_id',
        
    ];
}
