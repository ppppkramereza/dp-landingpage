<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Downlines extends Model
{
    use HasFactory;

    protected $table = 'downlines';

    protected $fillable = [
        'user_id',
        'downline_id',
        
    ];
}
