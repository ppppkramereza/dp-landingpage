<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Walletmt5 extends Model
{
    use HasFactory;

    protected $table = 'walletmt5s';

    protected $fillable = [
        'user_id',
        'balance',
        'last_update',
        'last_token'
    ];
}
