<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Models\User;
use App\Models\Downlines;
use App\Models\Uplines;
use App\Models\Wallet;
use App\Models\Walletmt5;
use Illuminate\Support\Facades\Mail;

class LandingController extends Controller
{
    public function beranda()
    {

        return view('landingpage/beranda');
    }
    public function about()
    {

        return view('landingpage/about');
    }
    public function contacts()
    {

        return view('landingpage/contacts');
    }
    public function news()
    {

        return view('landingpage/news');
    }
    public function ourteam()
    {

        return view('landingpage/ourteam');
    }
    public function broker()
    {

        return view('landingpage/broker');
    }
    public function privacy()
    {

        return view('landingpage/privacy');
    }

    public function product()
    {

        return view('landingpage/product');
    }
    public function register()
    {

        $pid = DB::SELECT("select*from phone_countries ");

        return view('landingpage/register', ['pid' => $pid]);
    }

    public function registeract(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $password = Str::random(8);
        $passwordhash = Hash::make($password);

        $country_code = $request->country_code;
        $phone = (int)$request->phone;
        $refferal = Str::random(8) . Carbon::now()->timestamp . Str::random(8);
        $refferaluplines = $request->refferalup;

        $checkphone = $phone;
        $sub_checkphone = substr($checkphone, 0, 1);


        $cim = DB::SELECT("select*from users where email='$email'");
        $cip = DB::SELECT("select*from users where phone='$phone'");
        $cid = DB::SELECT("select*from users where refferal='$refferaluplines'");
        if ($cim == null) {
            if ($cip == null) {
                if ($cid == null) {
                    return redirect('/register')->with(['error' => 'Refferal yang digunakan tidak tersedia']);
                } else {

                    $data = User::create([
                        'name' => $name,
                        'email' => $email,
                        'country_code' => $country_code,
                        'phone' => $phone,
                        'refferal' => $refferal,
                        'password' => $passwordhash,
                        'status' => 1,
                        'level_id' => 1,
                        'total_robot' => 0,
                        'modal_user' => 0,
                    ]);
                    $data->assignrole('Customer');

                    Wallet::create([
                        'user_id' => $data->id,
                        'balance' => 0,
                        'last_update' => Carbon::now(),
                        'last_token' => Str::random(60),
                    ]);

                    Walletmt5::create([
                        'user_id' => $data->id,
                        'balance' => 0,
                        'last_update' => Carbon::now(),
                        'last_token' => Str::random(60),
                    ]);

                    //$checkupl = DB::SELECT("select*from users a, uplines b where a.id = b.user.id and a.id = b.upline_id");
                    $checkupl = Uplines::create([
                        'user_id' => $data->id,
                        'upline_id' => $cid[0]->id

                    ]);
                    $checkudl = Downlines::create([
                        'user_id' => $cid[0]->id,
                        'downline_id' => $data->id

                    ]);


                    $details = [
                        'body' => $name,
                        'pass' => $password,
                        'email' => $email
                    ];

                    if ($country_code == 62) {
                        Mail::to($email)->send(new \App\Mail\MyTestMailIndo($details));
                    } else {
                        Mail::to($email)->send(new \App\Mail\MyTestMail($details));
                    }



                    return redirect('/verifikasiemail');
                }
            } else {
                return redirect('/register')->with(['error' => 'Nomor yang digunakan telah terdaftar / Format Penulisan Salah']);
            }
        } else {
            return redirect('/register')->with(['error' => 'Email yang digunakan telah terdaftar']);
        }
    }
    public function verifikasiemail()
    {

        return view('landingpage/verifikasiemail');
    }

    public function verif(Request $request, $email)
    {

        $data = DB::table('users')->where('email', $email)->update(['email_verified_at' => Carbon::now()]);

        return redirect()->away('https://play.google.com/store/apps/dev?id=8056989074181129540');
    }

    public function registerrefferal($refferal)
    {
        $data = User::where('refferal', $refferal)->first();
        $pid = DB::SELECT("select*from phone_countries ");
        if ($data) {
            return view('landingpage.register', ['pid' => $pid, 'refferal' => $data]);
        } else {
            return redirect('/register')->with(['error' => 'Refferal yang digunakan tidak terdaftar']);
        }
    }
}
