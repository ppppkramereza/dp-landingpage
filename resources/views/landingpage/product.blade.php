@extends('landingpage/base')
@section('topbar')

    <body id="home" class="version-2" style="background-color: #fff;">
        <!--===================== HEADER =====================-->
        <header class="header-two" style="background-color: #222222">
            <a href="#" class="nav-btn">
                <span style="background-color: white"></span>
                <span style="background-color: white"></span>
                <span style="background-color: white"></span>

            </a>

            <div class="header-menu header-menu-two">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="nav-menu">
                                <ul class="nav-list">
                                    <div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
                                        <a href="/" class="logo-footer">
                                            <img src="assets/img/dp1.svg" alt="logo">

                                            <div class="about-slogan-home-two" style="color: #FFD700">
                                                <p>dailypips</p>
                                            </div>
                                        </a>
                                    </div>

                                    <li><a href="/" style="color: white">Home</a></li>
                                    <li><a href="/about" style="color: white">About Us</a></li>
                                    <li><a href="/ourteam" style="color: white">Our Team</a></li>
                                    <li><a href="/product" style="color: #FFD700">Product</a></li>
                                    <li><a href="/broker" style="color: white">Broker</a></li>
                                    <li><a href="/contacts" style="color: white">Contacts</a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </header>
    @endsection

    @section('content')
        <div class="page-title" style="background:linear-gradient(124.13deg, #060606 -25.14%, #000a25 78.68%)">
            <div class="container">
                <h1 class="title-line-left">Product</h1>
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>Product</li>
                    </ul>
                </div>
            </div>
        </div>

        <section class="our-speakers speakers-home-two">
            <div class="bg-img"></div>
            <div class="container">
                <h2 class="title-line">AFBot</h2>
                <p class="slogan">Leave the old way and switch to a new way with Expert Advisor Robot</p>
                <div class="our-speakers-cover">
                    <div class="speaker-item">
                        <div class="speaker-item-img">
                            <img class="lazy" data-src="assets/img/robot1.svg" alt="img" style="width:95%;height:90%">
                        </div>
                        <div class="speaker-item-content">
                            <br><br>
                            <div class="prof"></div>
                            <h4 style="">Why should use afbot?</h4>

                            <p style="text-align: justify;color: #474747">1. Work automatically<br>
                                                            2. Without analysis<br>
                                                            3. Not wasting time<br>
                                                            3. Consistent profit</p><br>
                            <h4 style="color: red">Logic Robot</h4>
                            <p style="text-align: justify;color: #474747">- Daily goal 0% - 3%<br>
                                                            - Maximum cut loss 3%<br>
                                                            - Combines all the techniques that make profit possible,<br>
                                                        for example Averaging, Hedging, Martingale, One Shot, etc.</p>

                        </div>
                    </div>




                </div>
            </div>
        </section>
    @endsection
