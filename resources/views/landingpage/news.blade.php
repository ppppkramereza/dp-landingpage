@extends('landingpage/base')
@section('topbar')
<body id="home" class="version-2" style="background-color: #fff;">
	<!--===================== HEADER =====================-->
	<header class="header-two" style="background-color: #222222">
		<a href="#" class="nav-btn" >
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>
			
		</a>
		
		<div class="header-menu header-menu-two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<nav class="nav-menu">
							<ul class="nav-list">
								<div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
									<a href="/" class="logo-footer">
										<img src="assets/img/logopips2.svg" alt="logo">
									
									<div class="about-slogan-home-two" style="color: #FFD700"><p>dailypips</p></div>
									</a>
								</div>
								
								<li><a href="/" style="color: white">Home</a></li>
								<li><a href="/about" style="color: white">About Us</a></li>
								<li><a href="/ourteam" style="color: white">Our Team</a></li>
								<li><a href="/product" style="color: white">Product</a></li>
								<li><a href="/broker" style="color: white">Broker</a></li>
								<li><a href="/contacts" style="color: white">Contacts</a></li>
							</ul>
						</nav>
					</div>
					
				</div>
			</div>
		</div>
	</header>
	@endsection
	<!--=================== HEADER END ===================-->

	<!--=================== PAGE-TITLE ===================-->
	@section('content')
	<div class="page-title" style="background:linear-gradient(124.13deg, #38065d -25.14%, #946224 78.68%)">
		<div class="container">
			<h1 class="title-line-left">News</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="/">Home</a></li>
					<li>News</li>
				</ul>
			</div>
		</div>
	</div>
	<!--================= PAGE-TITLE END =================-->

	<!--===================== S-NEWS =====================-->
	<section class="s-news">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 blog-cover">
					<div class="post-item-cover">
						<div class="post-header">
							<h3 class="title title-line-left"><a href="single-news.html">Creating a Wordpress Blog for Beginners</a></h3>
							<div class="post-thumbnail">
								<a href="single-news.html">
									<img src="assets/img/news-1.jpg" alt="img">
								</a>
							</div>
							<div class="meta">
								<span class="post-by"><i class="fa fa-user" aria-hidden="true"></i>By <a href="#">Samson peters</a></span>
								<span class="post-date"><i class="fas fa-calendar-alt" aria-hidden="true"></i><a href="#">Dec 26, 2019</a></span>
								<span class="post-category"><i class="fas fa-tag" aria-hidden="true"></i><a href="#">Coaching</a></span>
							</div>
						</div>
						<div class="post-content">
							<div class="text">
								<p>Ultricies tristique nulla aliquet enim tortor. Arcu bibendum at varius vel pharetra vel turpis nunc eget. Et leo duis ut diam quam nulla. Cras pulvinar mattis nunc sed blandit libero volutpat. Blandit volutpat maecenas volutpat blandit aliquam etiam erat velit. </p>
							</div>
						</div>
					</div>
					<div class="post-item-cover">
						<div class="post-header">
							<h3 class="title title-line-left"><a href="single-news.html">Data Analysis with Pandas and Python</a></h3>
							<div class="post-thumbnail">
								<a href="single-news.html">
									<img src="assets/img/news-2.jpg" alt="img">
								</a>
							</div>
							<div class="meta">
								<span class="post-by"><i class="fa fa-user" aria-hidden="true"></i>By <a href="#">Samson peters</a></span>
								<span class="post-date"><i class="fas fa-calendar-alt" aria-hidden="true"></i><a href="#">Feb 26, 2019</a></span>
								<span class="post-category"><i class="fas fa-tag" aria-hidden="true"></i><a href="#">Coaching</a></span>
							</div>
						</div>
						<div class="post-content">
							<div class="text">
								<p>Ultricies tristique nulla aliquet enim tortor. Arcu bibendum at varius vel pharetra vel turpis nunc eget. Et leo duis ut diam quam nulla. Cras pulvinar mattis nunc sed blandit libero volutpat. Blandit volutpat maecenas volutpat blandit aliquam etiam erat velit. </p>
							</div>
						</div>
					</div>
					<!-- <div class="post-item-cover">
						<blockquote>
							<p>“At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias”</p>
							<cite>by <a href="#">Kerry Ashman</a></cite>
						</blockquote>
					</div> -->
					<div class="post-item-cover">
						<div class="post-header">
							<h3 class="title title-line-left"><a href="single-news.html">UX & Web Design Master Course: Strategy</a></h3>
							<div class="meta">
								<span class="post-by"><i class="fa fa-user" aria-hidden="true"></i>By <a href="#">Samson peters</a></span>
								<span class="post-date"><i class="fas fa-calendar-alt" aria-hidden="true"></i><a href="#">Feb 26, 2019</a></span>
								<span class="post-category"><i class="fas fa-tag" aria-hidden="true"></i><a href="#">Coaching</a></span>
							</div>
						</div>
						<div class="post-content">
							<div class="text">
								<p>Ultricies tristique nulla aliquet enim tortor. Arcu bibendum at varius vel pharetra vel turpis nunc eget. Et leo duis ut diam quam nulla. Cras pulvinar mattis nunc sed blandit libero volutpat. Blandit volutpat maecenas volutpat blandit aliquam etiam erat velit. </p>
							</div>
						</div>
					</div>
					<div class="pagination-cover">
						<ul class="pagination">
							<li class="pagination-item item-prev"><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
							<li class="pagination-item active"><a href="#">1</a></li>
							<li class="pagination-item"><a href="#">2</a></li>
							<li class="pagination-item"><a href="#">3</a></li>
							<li class="pagination-item"><a href="#">4</a></li>
							<li class="pagination-item"><a href="#">5</a></li>
							<li class="pagination-item item-next"><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
				<!--================= SIDEBAR =================-->
				<div class="col-12 col-lg-4 sidebar">
					<ul class="widgets">
						<!--=========== WIDGET-SEARCH ===========-->
						<li class="widget widget-search">
							<h6 class="title">search</h6>
							<form action="/" class="search-form">
								<input type="text" name="search" placeholder="Search">
								<button class="search-button" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
							</form>
						</li>
						<!--========= WIDGET-SEARCH END =========-->
						<!--=============== WIDGET-ARHIVE ===============-->
						<li class="widget widget-archive">
							<h6 class="title">archive</h6>
							<ul>
								<li><a href="#">January 2019</a></li>
								<li><a href="#">February 2019</a></li>
								<li><a href="#">March 2019</a></li>
								<li><a href="#">April 2019</a></li>
								<li><a href="#">May 2019</a></li>
							</ul>
						</li>
						<!--============ WIDGET-ARHIVE END ============-->
						<!--=========== WIDGET-CATEGORIES ===========-->
						<li class="widget widget-categories">
							<h6 class="title">categories</h6>
							<ul>
								<li><a href="#">Training</a></li>
								<li><a href="#">Courses</a></li>
								<li><a href="#">Conferences</a></li>
								<li><a href="#">Development</a></li>
								<li><a href="#">UI/UX Designer</a></li>
							</ul>
						</li>
						<!--========== WIDGET-CATEGORIES END ==========-->
						<!--=========== WIDGET-RECENT-POSTS ===========-->
						<li class="widget widget-recent-posts">
							<h6 class="title">recent blog posts</h6>
							<ul>
								<li>
									<a href="#">Mobile App Design: From Beginner to Intermediate</a>
									<div class="date"><i class="fas fa-calendar-alt" aria-hidden="true"></i>Dec 27, 2019 at 5:47 pm</div>
								</li>
								<li>
									<a href="#">Et harum quidem rerum facilis est et expedita distinctio</a>
									<div class="date"><i class="fas fa-calendar-alt" aria-hidden="true"></i>Dec 17, 2018 at 5:47 pm</div>
								</li>
								<li>
									<a href="#">Nam libero tempore, cum soluta nobis est eligendi optio</a>
									<div class="date"><i class="fas fa-calendar-alt" aria-hidden="true"></i>Dec 8, 2018 at 5:47 pm</div>
								</li>
							</ul>
						</li>
						<!--========== WIDGET-RECENT-POSTS END ==========-->
						<!--=========== WIDGET-INSTAGRAM ===========-->
						<li class="widget widget-instagram">
							<h6 class="title">Gallery</h6>
							<ul>
								<li>
									<a href="#"><img src="assets/img/gal-1.jpg" alt="img"></a>
								</li>
								<li>
									<a href="#"><img src="assets/img/gal-2.jpg" alt="img"></a>
								</li>
								<li>
									<a href="#"><img src="assets/img/gal-3.jpg" alt="img"></a>
								</li>
								<li>
									<a href="#"><img src="assets/img/gal-4.jpg" alt="img"></a>
								</li>
								<li>
									<a href="#"><img src="assets/img/gal-5.jpg" alt="img"></a>
								</li>
								<li>
									<a href="#"><img src="assets/img/gal-6.jpg" alt="img"></a>
								</li>
							</ul>
						</li>
						<!--=========== WIDGET-INSTAGRAM END ===========-->
						<!--=========== WIDGET-NEWSLETTER ===========-->
						<!-- <li class="widget widget-newsletter">
							<h6 class="title">newsletter</h6>
							<form action="/" class="subscribe-form">
								<input type="email" name="subscribe" placeholder="E-mail">
								<button class="search-button" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
							</form>
						</li> -->
						<!--=========== WIDGET-NEWSLETTER END ===========-->
					</ul>
				</div>
				<!--=============== SIDEBAR END ===============-->
			</div>
		</div>
	</section>
	
	<!--================= OUR-HISTORY END =================-->

	<!--=================== S-OUR-TEAM ===================-->
	@endsection