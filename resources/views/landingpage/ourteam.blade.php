@extends('landingpage/base')
@section('topbar')

    <body id="home" class="version-2" style="background-color: #fff;">
        <!--===================== HEADER =====================-->
        <header class="header-two" style="background-color: #222222">
            <a href="#" class="nav-btn">
                <span style="background-color: white"></span>
                <span style="background-color: white"></span>
                <span style="background-color: white"></span>

            </a>

            <div class="header-menu header-menu-two">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="nav-menu">
                                <ul class="nav-list">
                                    <div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
                                        <a href="/" class="logo-footer">
                                            <img src="assets/img/dp1.svg" alt="logo">

                                            <div class="about-slogan-home-two" style="color: #FFD700">
                                                <p>dailypips</p>
                                            </div>
                                        </a>
                                    </div>

                                    <li><a href="/" style="color: white">Home</a></li>
                                    <li><a href="/about" style="color: white">About Us</a></li>
                                    <li><a href="/ourteam" style="color: #FFD700">Our Team</a></li>
                                    <li><a href="/product" style="color: white">Product</a></li>
                                    <li><a href="/broker" style="color: white">Broker</a></li>
                                    <li><a href="/contacts" style="color: white">Contacts</a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </header>
    @endsection
    <!--=================== HEADER END ===================-->

    <!--=================== PAGE-TITLE ===================-->
    @section('content')
        <div class="page-title" style="background:linear-gradient(124.13deg, #060606 -25.14%, #000a25 78.68%)">
            <div class="container">
                <h1 class="title-line-left">Our Team</h1>
                <div class="breadcrumbs">
                    <ul>
                        <li><a href="/">Home</a></li>
                        <li>Our Team</li>
                    </ul>
                </div>
            </div>
        </div>
        <!--================= PAGE-TITLE END =================-->

        <!--==================== OVERVIEW ====================-->
        <section class="overview">
            <div class="container">
                <h2 class="title-line">Our Team</h2>
                <br><br>
                <div class="row">
                    <div class="team-wrap text-center w-100">
                        <div class="row justify-content-center">

                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <article class="team-box w-65 mr-auto ml-auto">
                                    <div class="team-img rounded-circle overflow-hidden">
                                        <a href="javascript:void(0);" title=""><img src="assets/img/team1.png"
                                                 alt="Team Image 2"></a>
                                    </div>
                                    <div class="team-info w-100 text-color12">
                                        <h3>Fendy Prasetyo</h3>
                                        <span class="">CEO</span>
                                        <div class="scl-lnks">
                                            <a href="javascript:void(0);" title="YouTube" target="_blank"><i
                                                    class="fab fa-youtube"></i></a>
                                            <a href="javascript:void(0);" title="Instagram" target="_blank"><i
                                                    class="fab fa-instagram"></i></a>
                                            <a href="javascript:void(0);" title="Twitter" target="_blank"><i
                                                    class="fab fa-twitter"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class="col-md-3 col-sm-6 col-lg-3">
                                <article class="team-box w-65 mr-auto ml-auto">
                                    <div class="team-img rounded-circle overflow-hidden">
                                        <a href="javascript:void(0);" title=""><img
                                                src="assets/img/team2.png" alt="Team Image 3"></a>
                                    </div>
                                    <div class="team-info w-100 text-color12">
                                        <h3>Sudirman A Latiev</h3>
                                        <span class="">COO</span>
                                        <div class="scl-lnks">
                                            <a href="javascript:void(0);" title="YouTube" target="_blank"><i
                                                    class="fab fa-youtube"></i></a>
                                            <a href="javascript:void(0);" title="Instagram" target="_blank"><i
                                                    class="fab fa-instagram"></i></a>
                                            <a href="javascript:void(0);" title="Twitter" target="_blank"><i
                                                    class="fab fa-twitter"></i></a>
                                        </div>
                                    </div>
                                </article>
                            </div>

                        </div>
                    </div><!-- Team Wrap -->
                </div>
            </div>
        </section>
        <!--================== OVERVIEW END ==================-->
        <!--================= OUR-HISTORY END =================-->
        <!-- <section class="our-history s-title-bg">
                                          <div class="container">
                                           <div class="row">
                                            <div class="col-12 col-sm-6">
                                             <div class="our-history-left">
                                              <h2 class="title-line-left">Our History</h2>
                                              <div class="overview-info-item">
                                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
                                               <p>Elementum nibh tellus molestie nunc non blandit massa enim. Pretium aenean pharetra magna ac placerat vestibulum lectus.</p>
                                              </div>
                                              <div class="overview-info-item">
                                               <h5>Our Certificates</h5>
                                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim. </p>
                                              </div>
                                              <div class="history-cert">
                                               <div class="history-cert-img">
                                                <a href="assets/img/certificate-1.jpg" data-fancybox="gallery2">
                                                 <img src="assets/img/certificate-1.jpg" alt="img">
                                                </a>
                                               </div>
                                               <div class="history-cert-img">
                                                <a href="assets/img/certificate-1.jpg" data-fancybox="gallery2">
                                                 <img src="assets/img/certificate-1.jpg" alt="img">
                                                </a>
                                               </div>
                                              </div>
                                             </div>
                                            </div>
                                            <div class="col-12 col-sm-6">
                                             <div class="history-info-cover">
                                              <div class="history-info">
                                               <h4 class="title"><span>2012-2014. </span>Our Establishment</h4>
                                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam. Elementum nibh tellus molestie.</p>
                                              </div>
                                              <div class="history-info">
                                               <h4 class="title"><span>2014. </span>The Early Days</h4>
                                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
                                              </div>
                                              <div class="history-info">
                                               <h4 class="title"><span>2015-2016. </span>Prosperity of Company</h4>
                                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
                                              </div>
                                              <div class="history-info">
                                               <h4 class="title"><span>2017-2018. </span>Worldwide Recognition</h4>
                                               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
                                              </div>
                                             </div>
                                            </div>
                                           </div>
                                          </div>
                                         </section> -->
        <!--================= OUR-HISTORY END =================-->

        <!--=================== S-OUR-TEAM ===================-->
    @endsection
