@extends('landingpage/base')
@section('topbar')
<body id="home" class="version-2" style="background-color: #fff;">
	<!--===================== HEADER =====================-->
	<header class="header-two" style="background-color: #222222">
		<a href="#" class="nav-btn" >
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>

		</a>

		<div class="header-menu header-menu-two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<nav class="nav-menu">
							<ul class="nav-list">
								<div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
									<a href="/" class="logo-footer">
										<img src="assets/img/dp1.svg" alt="logo">

									<div class="about-slogan-home-two" style="color: #FFD700"><p>dailypips</p></div>
									</a>
								</div>

								<li><a href="/" style="color: white">Home</a></li>
								<li><a href="/about" style="color: white">About Us</a></li>
								<li><a href="/ourteam" style="color: white">Our Team</a></li>
								<li><a href="/product" style="color: white">Product</a></li>
								<li><a href="/broker" style="color: white">Broker</a></li>
								<li><a href="/contacts" style="color: white">Contacts</a></li>
							</ul>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</header>
	@endsection
	<!--=================== HEADER END ===================-->

	<!--==================== MAIN-TWO ====================-->
	@section('content')

<div class="container">
	<section class="our-speakers speakers-home-two" style="">
			<h2 class="title-line"> Activate Your DailyPips Account</h2>
			<br>
			<p class="slogan">Thank you for registering in Dailypips. Please check your email to activate your dailypips account.</p>

		</div>
	</section>



@endsection
