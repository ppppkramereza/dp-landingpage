<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<title>Dailypips - The Investment Solution</title>
	<!-- ===================== META ===================== -->
	<meta name="keywords" content="dailypips">
	<meta name="description" content="The Investment Solution">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="{!! asset('assets/img/dp.jpg') !!}">
	<!-- ===================== STYLE ===================== -->
	<link rel="stylesheet" href="{!! asset('assets/css/slick.min.css') !!}">
	<link rel="stylesheet" href="{!! asset('assets/css/bootstrap-grid.css') !!}">
	<link rel="stylesheet" href="{!! asset('assets/css/bootstrap.min.css') !!}">
        <link rel="stylesheet" href="{!! asset('assets/css/bootstrap-select.min.css') !!}">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="https://unpkg.com/aos@2.3.1/dist/aos.css" />
	<link rel="stylesheet" href="{!! asset('assets/css/style.css') !!}">
	@section('script-header')
	@show
</head>


@section('topbar')
@show

@section('content')
		@show
	<!--================== S-LATEST-NEWS ==================-->
	<!-- <section class="s-latest-news">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					<h2 class="title-line-left">Let's Keep in Touch</h2>
				</div>
				<div class="col-12 col-md-6">
					<form action="#">
						<input type="email" name="your-email" placeholder="Your Email Address">
						<button class="btn btn-yellow" type="submit">Subscribe</button>
					</form>
				</div>
			</div>
		</div>
	</section> -->
	<!--================== S-LATEST-NEWS END ==================-->

	<!--===================== FOOTER =====================-->
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-4">
					<a href="/" class="logo-footer" >
						<img src="{!! asset('assets/img/dp1.svg') !!}" alt="logo">
						<div class="about-slogan-home-two" style="color: #FFD700;font-size: 25px"><p>dailypips</p></div>
					</a>
					<div class="footer-text" style="text-align: justify">Is an Expert advisor developer company that facilitate trading activities in the foreign exchange market with algorithms and AI patterns (Artificial Intelligence).</div>
					<ul class="soc-link">
						<li><a target="_blank" href="https://www.facebook.com/"><i class="fab fa-facebook-f"></i></a></li>
						<li><a target="_blank" href="https://twitter.com/"><i class="fab fa-twitter"></i></a></li>
						<li><a target="_blank" href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a></li>
						<li><a target="_blank" href="https://www.youtube.com"><i class="fab fa-youtube"></i></a></li>
					</ul>
                    <br>
                    <a href="/privacy" style="">Privacy Policy</a>
				</div>
				<div class="col-12 col-sm-6 col-md-4">
					<h6 style="color: #FFFFFF">Contacts</h6>
					<ul class="footer-contacts">
						<li>
							<i class="fas fa-map-marker-alt"></i>
							<a style="color: #FFFFFF">JL Pacar No 2 Desa/Kelurahan Ketabang, <br>Kecamatan Genteng, Kota Surabaya. Indonesia</a>
						</li>
						<li>
							<i class="fa fa-phone" aria-hidden="true"></i>
							<a style="color: #FFFFFF">081277288212</a>
						</li>
						<li>
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<a style="color: #FFFFFF">cs@dailypips.co</a>
						</li>
					</ul>
				</div>
				<div class="col-12 col-sm-6 col-md-4">
					<h6  style="color: #FFFFFF">Apps</h6>
					<ul class="">

					  <li><a href="https://play.google.com/store/apps/dev?id=8056989074181129540"><img class="lazy" src="assets/img/google-play.svg" data-src="assets/img/google-play.svg" alt="social"></a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="footer-bottom" style="color: ">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-4"><div class="copyright" style="color: white">© 2022. <a target="_blank" href="/" style="color: white">Dailypips</a>. All Rights Reserved.</div></div>
					<div class="col-8">

					</div>
				</div>
			</div>
		</div>
	</footer>
	<!--=================== FOOTER END ===================-->

	<!--===================== TO TOP =====================-->
	<a class="to-top" href="#home">
		<i class="fa fa-chevron-up" aria-hidden="true"></i>
	</a>
	<!--=================== TO TOP END ===================-->

	<!--===================== SCRIPT	=====================-->
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
	<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
	<script src="{!! asset('assets/js/slick.min.js') !!}"></script>
	<script src="{!! asset('assets/js/scripts.js') !!}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
  <script>
    AOS.init();

  </script>
	@section('script-js')
	@show
</body>
</html>
