@extends('landingpage/base')
@section('topbar')
<body id="home" class="version-2" style="background-color: #fff;">
	<!--===================== HEADER =====================-->
	<header class="header-two" style="background-color: #222222">
		<a href="#" class="nav-btn" >
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>

		</a>

		<div class="header-menu header-menu-two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<nav class="nav-menu">
							<ul class="nav-list">
								<div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
									<a href="/" class="logo-footer">
										<img src="assets/img/dp1.svg" alt="logo">

									<div class="about-slogan-home-two" style="color: #FFD700"><p>dailypips</p></div>
									</a>
								</div>

								<li><a href="/" style="color: #FFD700">Home</a></li>
								<li><a href="/about" style="color: white">About Us</a></li>
								<li><a href="/ourteam" style="color: white">Our Team</a></li>
								<li><a href="/product" style="color: white">Product</a></li>
								<li><a href="/broker" style="color: white">Broker</a></li>
								<li><a href="/contacts" style="color: white">Contacts</a></li>
							</ul>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</header>
	@endsection
	<!--=================== HEADER END ===================-->

	<!--==================== MAIN-TWO ====================-->
	@section('content')
	<section class="main-two">
		<div class="main-slider-two">
			<div class="main-slide-two" data-aos="fade-up" data-aos-delay="250"><!--
				<div class="main-slider-bg" style="background-image: url(assets/img/bg-slider-home2.svg);"></div> -->
				<img class="img-slide" src="assets/img/dpbanner.jpg" alt="img" style="margin-bottom: 180px;margin-right: 30px">
				<div class="container">
					<div class="main-slide-item">
						<!-- <div class="date-slide">January 17, 2019 / Washington DC</div> -->
						<h2 style="font-family:Open Sans;"><span>Welcome to</span><p style="font-size: 28px;font-family:Roboto">dailypips</p></h2>
						 <div class="slide-tag"></div>
						<div class="slide-btn-cover">
							<a href="/register" class="btn btn-yellow">Registration</a>
							<a href="https://play.google.com/store/apps/dev?id=8056989074181129540" class="btn btn-border">Login</a>
						</div>
					</div>
				</div>
			</div>
			<!-- <div class="main-slide-two">
				<div class="main-slide-date">January 17</div>
				<div class="main-slider-bg" style="background-image: url(assets/img/bg-slider-home2.svg);"></div>
				<img class="img-slide" src="assets/img/home-2-slide-2.png" alt="img">
				<div class="container">
					<div class="main-slide-item">
						<div class="date-slide">January 17, 2019 / Washington DC</div>
						<h2><span>Technology in</span>The Banking Sector</h2>
						<div class="slide-tag">#Web_Conference</div>
						<div class="slide-btn-cover">
							<a href="single-events.html" class="btn btn-yellow">register now</a>
							<a href="single-events.html" class="btn btn-border">more details</a>
						</div>
					</div>
				</div>
			</div>
			<div class="main-slide-two">
				<div class="main-slide-date">January 17</div>
				<div class="main-slider-bg" style="background-image: url(assets/img/bg-slider-home2.svg);"></div>
				<img class="img-slide" src="assets/img/home-2-slide-3.png" alt="img">
				<div class="container">
					<div class="main-slide-item">
						<div class="date-slide">January 17, 2019 / Washington DC</div>
						<h2><span>Ultimate Skills For an</span>Start in IT Recruiting</h2>
						<div class="slide-tag">#Web_Conference</div>
						<div class="slide-btn-cover">
							<a href="single-events.html" class="btn btn-yellow">register now</a>
							<a href="single-events.html" class="btn btn-border">more details</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="main-arrow-cover">
			<div class="container">
				<div class="main-slide-arrow"></div>
			</div>
		</div> -->
	</section>
	<!--=================== MAIN-TWO END ===================-->

	<!--==================== S-ABOUT ====================-->
	<!-- <section class="s-about-home-two">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6 about-info">
					<h2 class="title-line-left">More Than a Conference</h2>
					<div class="about-slogan-home-two">Lorem ipsum dolor sit amet, consectetur adipiscing elit,  sed do eiusmod tempor incididunt ut labore.</div>
					<div class="about-info-text">
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusm od tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					</div>
					<ul class="soc-link">
						<li><a target="_blank" href="https://www.facebook.com/rovadex"><i class="fab fa-facebook-f"></i></a></li>
						<li><a target="_blank" href="https://www.instagram.com/rovadex"><i class="fab fa-instagram"></i></a></li>
						<li><a target="_blank" href="https://www.youtube.com"><i class="fab fa-youtube"></i></a></li>
						<li><a target="_blank" href="https://vimeo.com/"><i class="fab fa-vimeo"></i></a></li>
					</ul>
					<a href="events.html" class="btn btn-yellow">read more</a>
				</div>
				<div class="col-12 col-md-6 about-img">
					<a data-fancybox href="https://www.youtube.com/watch?v=_sI_Ps7JSEk&amp;autoplay=1&amp;rel=0&amp;controls=0&amp;showinfo=0">
						<img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/news-2.jpg" alt="img">
					</a>
				</div>
			</div>
		</div>
	</section> -->
	<!--================== S-ABOUTK END ==================-->

	<!--================== OUR-SPEAKERS ==================-->

	<!--================ OUR-SPEAKERS END ================-->

	<!--================= S-SCHEDULE-EVENT =================-->
	<!-- <section class="s-schedule-event schedule-event-home-two">
		<div class="container">
			<h2 class="title-line">Schedule of Event</h2>
			<p class="slogan">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
			<ul class="schedule-cover">
				<li class="active">
					<div class="schedule-header">
						<div class="schedule-time">10:00-11:00</div>
						<div class="schedule-title">Check-in. morning coffee</div>
						<i class="fa fa-chevron-left" aria-hidden="true"></i>
					</div>
					<div class="schedule-content">
						<h3 class="title">Good Service: Detect and avoid errors in the world of microservices</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</li>
				<li>
					<div class="schedule-header">
						<div class="schedule-time">11:00-12:00</div>
						<div class="schedule-title">Anthony Watson</div>
						<i class="fa fa-chevron-left" aria-hidden="true"></i>
					</div>
					<div class="schedule-content">
						<h3 class="title">Good Service: Detect and avoid errors in the world of microservices</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</li>
				<li>
					<div class="schedule-header">
						<div class="schedule-time">12:00-13:00</div>
						<div class="schedule-title">lorem ipsum</div>
						<i class="fa fa-chevron-left" aria-hidden="true"></i>
					</div>
					<div class="schedule-content">
						<h3 class="title">Good Service: Detect and avoid errors in the world of microservices</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</li>
				<li>
					<div class="schedule-header">
						<div class="schedule-time">13:00-14:00</div>
						<div class="schedule-title">dolor sit amet</div>
						<i class="fa fa-chevron-left" aria-hidden="true"></i>
					</div>
					<div class="schedule-content">
						<h3 class="title">Good Service: Detect and avoid errors in the world of microservices</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</li>
				<li>
					<div class="schedule-header">
						<div class="schedule-time">14:00-15:00</div>
						<div class="schedule-title">sed do eiusmod tempor incididunt</div>
						<i class="fa fa-chevron-left" aria-hidden="true"></i>
					</div>
					<div class="schedule-content">
						<h3 class="title">Good Service: Detect and avoid errors in the world of microservices</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
					</div>
				</li>
			</ul>
		</div>
	</section> -->
	<!--=============== S-SCHEDULE-EVENT END ===============-->

	<!--===================== S-PRICING =====================-->
	<!-- <section class="s-pricing">
		<div class="container">
			<h2 class="title-line">Pricing Plans</h2>
			<p class="slogan">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.</p>
			<div class="pricing-cover">
				<div class="pricing-item">
					<h4 class="pricing-header">Klasik</h4>
					<div class="pricing-item-content">
						<div class="price">$120</div>
						<div class="pricing-slogan title-line">access to the conference</div>
						<div class="pricing-info">
							<ul>
								<li>Conference Ticket</li>
								<li>Coffee Break</li>
								<li>Branded T-shirt</li>
							</ul>
						</div>
						<a href="contacts.html" class="btn btn-yellow">Buy Now</a>
					</div>
				</div>
				<div class="pricing-item">
					<h4 class="pricing-header">Ruby</h4>
					<div class="pricing-item-content">
						<div class="price">$150</div>
						<div class="pricing-slogan title-line">access to the conference</div>
						<div class="pricing-info">
							<ul>
								<li>Conference Ticket</li>
								<li>Coffee Break</li>
								<li>Branded T-shirt</li>
								<li>Afterparty</li>
							</ul>
						</div>
						<a href="contacts.html" class="btn btn-yellow">Buy Now</a>
					</div>
				</div>
				<div class="pricing-item">
					<h4 class="pricing-header">Saphhire</h4>
					<div class="pricing-item-content">
						<div class="price">$80</div>
						<div class="pricing-slogan title-line">access to the conference</div>
						<div class="pricing-info">
							<p>When you purchase this package, you get an online conference recording</p>
						</div>
						<a href="contacts.html" class="btn btn-yellow">Buy Now</a>
					</div>
				</div>
			</div>
			<br>
			<br>
			<div class="pricing-cover">
				<div class="pricing-item">
					<h4 class="pricing-header">Emerald</h4>
					<div class="pricing-item-content">
						<div class="price">$120</div>
						<div class="pricing-slogan title-line">access to the conference</div>
						<div class="pricing-info">
							<ul>
								<li>Conference Ticket</li>
								<li>Coffee Break</li>
								<li>Branded T-shirt</li>
							</ul>
						</div>
						<a href="contacts.html" class="btn btn-yellow">Buy Now</a>
					</div>
				</div>
				<div class="pricing-item">
					<h4 class="pricing-header">Diamond</h4>
					<div class="pricing-item-content">
						<div class="price">$150</div>
						<div class="pricing-slogan title-line">access to the conference</div>
						<div class="pricing-info">
							<ul>
								<li>Conference Ticket</li>
								<li>Coffee Break</li>
								<li>Branded T-shirt</li>
								<li>Afterparty</li>
							</ul>
						</div>
						<a href="contacts.html" class="btn btn-yellow">Buy Now</a>
					</div>
				</div>
				<div class="pricing-item">
					<h4 class="pricing-header">Black Pearl</h4>
					<div class="pricing-item-content">
						<div class="price">$80</div>
						<div class="pricing-slogan title-line">access to the conference</div>
						<div class="pricing-info">
							<p>When you purchase this package, you get an online conference recording</p>
						</div>
						<a href="contacts.html" class="btn btn-yellow">Buy Now</a>
					</div>
				</div>
			</div>
		</div>
	</section> -->
<br><br><br>
	<!-- <section class="s-testimonials testimonials-slider-cover s-title-bg">

		<div class="container">
			<h2 class="title-line">Testimonials</h2>
			<p class="slogan">They possess the secret knowledge and interesting experience of creating a digital product.</p>
			<div class="slider-testimonials">
				<div class="slide-testimonial">
					<div class="testimonial-item">
						<img src="assets/img/testimonials-1.jpg" alt="img">
						<span class="slide-quote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
						<div class="testimon-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam. Elementum nibh tellus molestie nunc non</div>
						<h4 class="name">Louisa Russell</h4>
						<div class="prof">Regular Client</div>
					</div>
				</div>
				<div class="slide-testimonial">
					<div class="testimonial-item">
						<img src="assets/img/testimonials-2.jpg" alt="img">
						<span class="slide-quote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
						<div class="testimon-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo mollitia vitae architecto fuga molestias ad voluptates beatae error doloribus reprehenderit.</div>
						<h4 class="name">Samson Peters</h4>
						<div class="prof">Regular Client</div>
					</div>
				</div>
				<div class="slide-testimonial">
					<div class="testimonial-item">
						<img src="assets/img/testimonials-3.jpg" alt="img">
						<span class="slide-quote"><i class="fa fa-quote-left" aria-hidden="true"></i></span>
						<div class="testimon-content">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sequi atque inventore pariatur itaque, maxime voluptatibus iusto quam hic beatae labore.</div>
						<h4 class="name">Marry James</h4>
						<div class="prof">Regular Client</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!--================== S-PRICING END ==================-->

	<!--===================== S-VENUE =====================-->
	<!-- <section class="s-venue">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-4">
					<h2 class="title-line-left">Venue</h2>
					<ul>
						<li><i class="fas fa-map-marker-alt"></i>7100 Athens Place Washington, DC 20521</li>
						<li><i class="fas fa-street-view"></i>AMA Conference Center Hall #1, 6th floor</li>
					</ul>
				</div>
				<div class="col-12 col-sm-8"><img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/venue-img.jpg" alt="img"></div>
			</div>
		</div>
	</section> -->
	<!--=================== S-VENUE END ===================-->

	<!--==================== S-PARTNERS ====================-->
	<!-- <section class="s-partners partners-home-two">
		<div class="container">
			<div class="row">
				<div class="col-6 col-sm-4 col-md-2"><img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/home-2-partner-1.png" alt="img"></div>
				<div class="col-6 col-sm-4 col-md-2"><img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/home-2-partner-2.png" alt="img"></div>
				<div class="col-6 col-sm-4 col-md-2"><img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/home-2-partner-3.png" alt="img"></div>
				<div class="col-6 col-sm-4 col-md-2"><img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/home-2-partner-4.png" alt="img"></div>
				<div class="col-6 col-sm-4 col-md-2"><img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/home-2-partner-5.png" alt="img"></div>
				<div class="col-6 col-sm-4 col-md-2"><img class="lazy" src="assets/img/placeholder-all.png" data-src="assets/img/home-2-partner-6.png" alt="img"></div>
			</div>
		</div>
	</section> -->
	<!--================== S-PARTNERS END ==================-->

	<!--================== S-LATEST-NEWS ==================-->

	<!--================== S-PARTNERS END ==================-->
@endsection
