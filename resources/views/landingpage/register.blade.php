@extends('landingpage/base')
@section('script-header')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
@endsection
@section('topbar')

    <body id="home" class="version-2" style="background-color: #fff;">
        <!--===================== HEADER =====================-->
        <header class="header-two" style="background-color: #222222">
            <a href="#" class="nav-btn">
                <span style="background-color: white"></span>
                <span style="background-color: white"></span>
                <span style="background-color: white"></span>

            </a>

            <div class="header-menu header-menu-two">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="nav-menu">
                                <ul class="nav-list">
                                    <div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
                                        <a href="/" class="logo-footer">
                                            <img src="{!! asset('assets/img/dp1.svg') !!}" alt="logo">

                                            <div class="about-slogan-home-two" style="color: #FFD700">
                                                <p>dailypips</p>
                                            </div>
                                        </a>
                                    </div>

                                    <li><a href="/" style="color: white">Home</a></li>
                                    <li><a href="/about" style="color: white">About Us</a></li>
                                    <li><a href="/ourteam" style="color: white">Our Team</a></li>
                                    <li><a href="/product" style="color: white">Product</a></li>
                                    <li><a href="/broker" style="color: white">Broker</a></li>
                                    <li><a href="/contacts" style="color: white">Contacts</a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </header>
    @endsection
    <!--=================== HEADER END ===================-->

    <!--==================== MAIN-TWO ====================-->
    @section('content')
        <section class="page-contacts">
            <div class="container">
                <h2 class="title-line-left">New Member</h2>
                <div class="row">
                    <div class="col-12 col-sm-8">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        @if ($message = Session::get('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        @if ($message = Session::get('warning'))
                            <div class="alert alert-warning alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        @if ($message = Session::get('info'))
                            <div class="alert alert-info alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                Please check the form below for errors
                            </div>
                        @endif

                        <p>Please fill in this form to create an account.</p>
                        <hr>
                        <form action="/registeract" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-12">
                                <label for="email"><b>Name</b></label>
                                <input type="text" placeholder="Enter Username" name="name" id="name" required>
                            </div>

                            <div class="col-md-12">
                                <label for="email"><b>E-mail</b></label>
                                <input type="text" placeholder="Enter Email" name="email" id="email" required>
                            </div>



                            <div class="col-md-3">
                                <label for="country_code"><b>Phone Code</b></label>

                                <select class="form-control" id="country_code" name="country_code" style="height: 50px">

                                    <option value="62">Indonesia (+62)</option>
                                    @foreach ($pid as $ccode)
                                        <option value="{{ $ccode->iso_code }}">{{ $ccode->name }}
                                            (+{{ $ccode->iso_code }})
                                        </option>
                                    @endforeach
                                </select>

                            </div>

                            <div class="col-md-9">
                                <label for="country_code"><b>Phone Number</b></label>
                                <input type="text" placeholder="Enter Phone" name="phone" id="phone" required ">
                   </div>

                   <div class="   col-md-12">
                                <label for="email"><b>Referral</b></label>
                                <input type="text" placeholder="Enter Referral" name="refferalup" id="refferal" required
                                    value="{{ $refferal->refferal ?? '' }}">
                            </div>


                            <hr>
                            <div class="col-md-12">

                                <p>By creating an account you agree to our <a href="/privacy">Terms & Privacy</a>.</p>
                                <br>

                                <button type="submit" class="btn btn-yellow">Register</button>
                            </div>


                        </form>
                    </div>


                </div>
            </div>

        </section>
    @endsection

    @section('script-js')
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    @endsection
