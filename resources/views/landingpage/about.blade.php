@extends('landingpage/base')
@section('topbar')
<body id="home" class="version-2" style="background-color: #fff;">
	<!--===================== HEADER =====================-->
	<header class="header-two" style="background-color: #222222">
		<a href="#" class="nav-btn" >
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>

		</a>

		<div class="header-menu header-menu-two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<nav class="nav-menu">
							<ul class="nav-list">
								<div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
									<a href="/" class="logo-footer">
										<img src="assets/img/dp1.svg" alt="logo">

									<div class="about-slogan-home-two" style="color: #FFD700"><p>dailypips</p></div>
									</a>
								</div>

								<li><a href="/" style="color: white">Home</a></li>
								<li><a href="/about" style="color: #FFD700">About Us</a></li>
								<li><a href="/ourteam" style="color: white">Our Team</a></li>
								<li><a href="/product" style="color: white">Product</a></li>
								<li><a href="/broker" style="color: white">Broker</a></li>
								<li><a href="/contacts" style="color: white">Contacts</a></li>
							</ul>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</header>
	@endsection
	<!--=================== HEADER END ===================-->

	<!--=================== PAGE-TITLE ===================-->
	@section('content')
	<div class="page-title" style="background:linear-gradient(124.13deg, #060606 -25.14%, #000a25 78.68%)">
		<div class="container">
			<h1 class="title-line-left">About Us</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="/">Home</a></li>
					<li>About Us</li>
				</ul>
			</div>
		</div>
	</div>
	<!--================= PAGE-TITLE END =================-->

	<!--==================== OVERVIEW ====================-->
	<section class="overview">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="overview-info">
						<h2 class="title-line-left">Overview</h2>
                        <div class="overview-info-item">
							<h4 style="font-size: 16pt">Vision</h4>
							<p style="color: #313131;text-align: justify;">
								Building and creating a productive generation in today's digital era
						</div>
                        <div class="overview-info-item">
							<h4 style="font-size: 16pt">Mission</h4>
							<p style="color: #313131;text-align: justify;">
								Educating the nation's life and presenting financial freedom solutions for the future
						</div>
						<div class="overview-info-item">
							<h4>Dailypips</h4>
							<p style="color: #313131;text-align: justify;">In the world of forex, trading robots, or what are often called EAs (expert advisors) are well known
								by traders. Although the majority of users still use the manual method, but not a few
								who likes to use trading robots.
								<br><br>
								DAILYPIPS understands that in investing, there are so many things that must be analyzed so that
								investors get maximum returns. Will the price wait for us to finish doing
								research, just moving? The answer is of course NO.
								<br><br>
								With an accuracy rate of 70%, the DAILYPIPS method can generate trend direction information,
								price movement, until the time of the movement. It can be given,
								before the price moves, before the moment passes and it's too late for action.
								<br><br>
								The DAILYPIPS method creates solutions and delivers results. We create solutions that
								according to the needs, scale of business and the market where you are. We keep on innovating
								to get better every year.
								<br><br>
								Some people think that trading is an investment instrument with a high level of risk
								tall one. We do not agree with this paradigm because the risk will always be there
								investment and business world, but good ability to understand and analyze
								the instruments used will reduce and minimize the risks that may occur. here
								which is why we founded DAILYPIPS</p>
						</div>
						<br>
						<!-- <h2 class="title-line-left">Our Mission</h2> -->
						<div class="overview-info-item">
							<h4>PT. Bangkit Digital Indonesia</h4>
							<p style="color: #313131;text-align: justify;">
								Is an Expert advisor developer company that
								facilitate trading activities in the foreign exchange market with algorithms and AI patterns
								(Artificial Intelligence).
								<br><br>
								Established since 2021, PT. Bangkit Digital Indonesia has a mission to generate digital finance in collaboration with brokers and traders from all over the world.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6 overview-img-cover">
					<div class="overview-img-cover">
						<div class="overview-img" style="padding-top: 120px">
							<img src="assets/img/logopips2.jpeg" alt="img">
							  <img src="assets/img/s1-blur.png" alt="img">
							  <img src="assets/img/s2-blur.png" alt="img">


						</div>
						<div class="overview-img" style="height: 500px; padding-top: 200px">
							<img src="assets/img/s3-blur.png" alt="img">
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================== OVERVIEW END ==================-->
	<!--================= OUR-HISTORY END =================-->
	<!-- <section class="our-history s-title-bg">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6">
					<div class="our-history-left">
						<h2 class="title-line-left">Our History</h2>
						<div class="overview-info-item">
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
							<p>Elementum nibh tellus molestie nunc non blandit massa enim. Pretium aenean pharetra magna ac placerat vestibulum lectus.</p>
						</div>
						<div class="overview-info-item">
							<h5>Our Certificates</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim. </p>
						</div>
						<div class="history-cert">
							<div class="history-cert-img">
								<a href="assets/img/certificate-1.jpg" data-fancybox="gallery2">
									<img src="assets/img/certificate-1.jpg" alt="img">
								</a>
							</div>
							<div class="history-cert-img">
								<a href="assets/img/certificate-1.jpg" data-fancybox="gallery2">
									<img src="assets/img/certificate-1.jpg" alt="img">
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6">
					<div class="history-info-cover">
						<div class="history-info">
							<h4 class="title"><span>2012-2014. </span>Our Establishment</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam. Elementum nibh tellus molestie.</p>
						</div>
						<div class="history-info">
							<h4 class="title"><span>2014. </span>The Early Days</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
						</div>
						<div class="history-info">
							<h4 class="title"><span>2015-2016. </span>Prosperity of Company</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
						</div>
						<div class="history-info">
							<h4 class="title"><span>2017-2018. </span>Worldwide Recognition</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod aliqua. Ut enim ad minim veniam.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!--================= OUR-HISTORY END =================-->

	<!--=================== S-OUR-TEAM ===================-->
	@endsection
