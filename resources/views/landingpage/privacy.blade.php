@extends('landingpage/base')
@section('topbar')
<body id="home" class="version-2" style="background-color: #fff;">
	<!--===================== HEADER =====================-->
	<header class="header-two" style="background-color: #222222">
		<a href="#" class="nav-btn" >
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>

		</a>

		<div class="header-menu header-menu-two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<nav class="nav-menu">
							<ul class="nav-list">
								<div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
									<a href="/" class="logo-footer">
										<img src="assets/img/dp1.svg" alt="logo">

									<div class="about-slogan-home-two" style="color: #FFD700"><p style="text-align: justify;color: #65584b">dailypips</p></div>
									</a>
								</div>

								<li><a href="/" style="color: white">Home</a></li>
								<li><a href="/about" style="color: white">About Us</a></li>
								<li><a href="/ourteam" style="color: white">Our Team</a></li>
								<li><a href="/product" style="color: white">Product</a></li>
								<li><a href="/broker" style="color: white">Broker</a></li>
								<li><a href="/contacts" style="color: white">Contacts</a></li>
							</ul>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</header>
	@endsection
	<!--=================== HEADER END ===================-->

	<!--==================== MAIN-TWO ====================-->
	@section('content')
	<div class="page-title" style="background:linear-gradient(124.13deg, #060606 -25.14%, #000a25 78.68%)">
		<div class="container">
			<h1 class="title-line-left">Privacy Policy</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="/">Home</a></li>
					<li>Privacy Policy</li>
				</ul>
			</div>
		</div>
	</div>
	<!--================= PAGE-TITLE END =================-->

    <section class="s-pravicy-policy">
		<div class="container">
			<div class="accordion-wrap">
				<div class="accordion-item">
					<h3 class="title title-line-left">Privacy Policy for PT Bangkit Digital Indonesia</h3>
					<div class="accordion-content">
						<p style="text-align: justify;color: #65584b">At dailypips, accessible from https://dailypips.co/, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by dailypips and how we use it.</p>
                        <br>
                        <p style="text-align: justify;color: #65584b">If you have additional questions or require more information about our Privacy Policy, do not hesitate to contact us.</p>
                        <br>
                        <p style="text-align: justify;color: #65584b">This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in dailypips. This policy is not applicable to any information collected offline or via channels other than this website. Our Privacy Policy was created with the help of the <a href="https://www.privacypolicygenerator.info">Free Privacy Policy Generator</a>.</p>

                    </div>
				</div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">Consent</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">By using our website, you hereby consent to our Privacy Policy and agree to its terms.</p>
                </div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">Information we collect</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">The personal information that you are asked to provide, and the reasons why you are asked to provide it, will be made clear to you at the point we ask you to provide your personal information.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">If you contact us directly, we may receive additional information about you such as your name, email address, phone number, the contents of the message and/or attachments you may send us, and any other information you may choose to provide.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">When you register for an Account, we may ask for your contact information, including items such as name, company name, address, email address, and telephone number.</p>
                </div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">How we use your information</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">We use the information we collect in various ways, including to:</p>
                    <ul>
                        <li>Provide, operate, and maintain our website</li>
                        <li>Improve, personalize, and expand our website</li>
                        <li>Understand and analyze how you use our website</li>
                        <li>Develop new products, services, features, and functionality</li>
                        <li>Communicate with you, either directly or through one of our partners, including for customer service, to provide you with updates and other information relating to the website, and for marketing and promotional purposes</li>
                        <li>Send you emails</li>
                        <li>Find and prevent fraud</li>
                        </ul>
                </div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">Log Files</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">dailypips follows a standard procedure of using log files. These files log visitors when they visit websites. All hosting companies do this and a part of hosting services' analytics. The information collected by log files include internet protocol (IP) addresses, browser type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the number of clicks. These are not linked to any information that is personally identifiable. The purpose of the information is for analyzing trends, administering the site, tracking users' movement on the website, and gathering demographic information.</p>
                </div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">Advertising Partners Privacy Policies</h3>
				<div class="accordion-content">
					<p style="text-align: justify;color: #65584b">You may consult this list to find the Privacy Policy for each of the advertising partners of dailypips.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons that are used in their respective advertisements and links that appear on dailypips, which are sent directly to users' browser. They automatically receive your IP address when this occurs. These technologies are used to measure the effectiveness of their advertising campaigns and/or to personalize the advertising content that you see on websites that you visit.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">Note that dailypips has no access to or control over these cookies that are used by third-party advertisers.</p>
				</div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">Third Party Privacy Policies</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">dailypips's Privacy Policy does not apply to other advertisers or websites. Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers for more detailed information. It may include their practices and instructions about how to opt-out of certain options. </p>
                    <br>
                    <p style="text-align: justify;color: #65584b">You can choose to disable cookies through your individual browser options. To know more detailed information about cookie management with specific web browsers, it can be found at the browsers' respective websites.</p>

                </div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">CCPA Privacy Rights (Do Not Sell My Personal Information)</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">Under the CCPA, among other rights, California consumers have the right to:</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">Request that a business that collects a consumer's personal data disclose the categories and specific pieces of personal data that a business has collected about consumers.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">Request that a business delete any personal data about the consumer that a business has collected.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">Request that a business that sells a consumer's personal data, not sell the consumer's personal data.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.</p>

                </div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">GDPR Data Protection Rights</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">We would like to make sure you are fully aware of all of your data protection rights. Every user is entitled to the following:</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">The right to access – You have the right to request copies of your personal data. We may charge you a small fee for this service.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">The right to rectification – You have the right to request that we correct any information you believe is inaccurate. You also have the right to request that we complete the information you believe is incomplete.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">The right to erasure – You have the right to request that we erase your personal data, under certain conditions.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">The right to restrict processing – You have the right to request that we restrict the processing of your personal data, under certain conditions.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">The right to object to processing – You have the right to object to our processing of your personal data, under certain conditions.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">The right to data portability – You have the right to request that we transfer the data that we have collected to another organization, or directly to you, under certain conditions.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">If you make a request, we have one month to respond to you. If you would like to exercise any of these rights, please contact us.</p>

                </div>
			</div>
			<div class="accordion-item">
				<h3 class="title title-line-left">Children's Information</h3>
				<div class="accordion-content">
                    <p style="text-align: justify;color: #65584b">Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.</p>
                    <br>
                    <p style="text-align: justify;color: #65584b">dailypips does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.</p>
                </div>
			</div>

		</div>
	</section>



@endsection
