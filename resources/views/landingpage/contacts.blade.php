@extends('landingpage/base')
@section('topbar')
<body id="home" class="version-2" style="background-color: #fff;">
	<!--===================== HEADER =====================-->
	<header class="header-two" style="background-color: #222222">
		<a href="#" class="nav-btn" >
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>
			<span style="background-color: white"></span>

		</a>

		<div class="header-menu header-menu-two">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<nav class="nav-menu">
							<ul class="nav-list">
								<div class="col-4 col-md-8 col-lg-3 logo-two-cover" style="margin-right: 150px">
									<a href="/" class="logo-footer">
										<img src="assets/img/dp1.svg" alt="logo">

									<div class="about-slogan-home-two" style="color: #FFD700"><p>dailypips</p></div>
									</a>
								</div>

								<li><a href="/" style="color: white">Home</a></li>
								<li><a href="/about" style="color: white">About Us</a></li>
								<li><a href="/ourteam" style="color: white">Our Team</a></li>
								<li><a href="/product" style="color: white">Product</a></li>
								<li><a href="/broker" style="color: white">Broker</a></li>
								<li><a href="/contacts" style="color: #FFD700">Contacts</a></li>
							</ul>
						</nav>
					</div>

				</div>
			</div>
		</div>
	</header>
	@endsection
	<!--=================== HEADER END ===================-->

	<!--==================== MAIN-TWO ====================-->
	@section('content')
	<div class="page-title" style="background:linear-gradient(124.13deg, #060606 -25.14%, #000a25 78.68%)">
		<div class="container">
			<h1 class="title-line-left">Contacts</h1>
			<div class="breadcrumbs">
				<ul>
					<li><a href="/">Home</a></li>
					<li>Contacts</li>
				</ul>
			</div>
		</div>
	</div>
	<!--================= PAGE-TITLE END =================-->

	<!--================= PAGE-CONTACTS =================-->
	<section class="page-contacts">
		<div class="container">
			<h2 class="title-line-left">Get in Touch</h2>
			<div class="row">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3957.8582736510216!2d112.7471628356976!3d-7.25696628661979!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7f936ede48383%3A0x8d1fd95b186f9642!2sMau%2C%20Jl.%20Pacar%20No.2%2C%20Ketabang%2C%20Kec.%20Genteng%2C%20Kota%20SBY%2C%20Jawa%20Timur%2060272!5e0!3m2!1sid!2sid!4v1641572952503!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

				<div class="col-12 col-sm-4 page-cont-info" style="margin-left: 50px">
					<div class="cont-info-item">
						<i class="fas fa-map-marker-alt"></i>
						<h5>Address</h5>
						<p>Jl. Pacar No 2 Ketabang, <br>Genteng,Surabaya, East Java Indonesia</p>
					</div>
					<div class="cont-info-item">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<h5>Phones</h5>
						<ul class="cont-phone">
							<li><a>+62 812 7728 8212</a></li>
						</ul>
					</div>
					<div class="cont-info-item">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<h5>Email</h5>
						<ul class="cont-email">
							<li><a>cs@dailypips.co</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================= PAGE-CONTACTS END =================-->


@endsection
